package com.diachenko.coding.task;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.diachenko.coding.task.formater.CategoryItemsProcessor;
import com.diachenko.coding.task.formater.TestFormaterUtil;
import com.diachenko.coding.task.output.OutputWriter;
import com.diachenko.coding.task.strategy.CategoryDataConsumerStrategy;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationTest {
    @Mock
    private ApplicationContext applicationContext;
    @Mock
    private OutputWriter outputWriter;
    @Mock
    private ApplicationStartParameters applicationStartParameters;

    @Spy
    @InjectMocks
    private Application application;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(applicationContext.getOutputWriter()).thenReturn(outputWriter);
        doNothing().when(application).printResults();
    }

    @Test
    public void shouldStartFileHandling() {
        String expectedFilePath = "filePath";
        Optional<String> expectedFilePathOptional = Optional.of(expectedFilePath);
        when(applicationContext.getStartParameters()).thenReturn(applicationStartParameters);
        when(applicationStartParameters.getFilePath()).thenReturn(expectedFilePathOptional);

        application.start();

        verify(application).handleFile(expectedFilePath);
    }

    @Test
    public void shouldNotStartFileHandling() {
        Optional<String> expectedFilePathOptional = Optional.empty();
        when(applicationContext.getStartParameters()).thenReturn(applicationStartParameters);
        when(applicationStartParameters.getFilePath()).thenReturn(expectedFilePathOptional);

        application.start();

        verify(application, never()).handleFile(anyString());
    }

    @Test
    public void shouldFoundDataConsumerStrategyForHeader() throws IOException {
        final Path input = Files.createTempFile("input", ".txt");
        final String absolutePath = input.toFile().getAbsolutePath();
        final String inputFileContent = TestFormaterUtil.joinLinesWithLineSeparator("ANIMALS");
        Files.write(input, inputFileContent.getBytes());

        final CategoryDataConsumerStrategy categoryDataConsumerStrategy = mockCategoryDataConsumerStrategy("ANIMALS");
        List<CategoryDataConsumerStrategy> strategies = Collections.singletonList(categoryDataConsumerStrategy);
        when(applicationContext.getStrategies()).thenReturn(strategies);

        application.handleFile(absolutePath);

        verify(categoryDataConsumerStrategy, never()).add(anyString());
    }

    @Test
    public void shouldParseAndConsumeCategoryElements() throws IOException {
        final Path input = Files.createTempFile("input", ".txt");
        final String absolutePath = input.toFile().getAbsolutePath();
        final String inputFileContent = TestFormaterUtil.joinLinesWithLineSeparator("ANIMALS", "cow", "dog", "cat");
        Files.write(input, inputFileContent.getBytes());

        final CategoryDataConsumerStrategy categoryDataConsumerStrategy = mockCategoryDataConsumerStrategy("ANIMALS");
        List<CategoryDataConsumerStrategy> strategies = Collections.singletonList(categoryDataConsumerStrategy);
        when(applicationContext.getStrategies()).thenReturn(strategies);

        application.handleFile(absolutePath);

        verify(categoryDataConsumerStrategy).add(eq("cow"));
        verify(categoryDataConsumerStrategy).add(eq("dog"));
        verify(categoryDataConsumerStrategy).add(eq("cat"));
    }

    @Test
    public void shouldParseAndConsumeTwoCategoriesElements() throws IOException {
        final Path input = Files.createTempFile("input", ".txt");
        final String absolutePath = input.toFile().getAbsolutePath();
        final String inputFileContent = TestFormaterUtil.joinLinesWithLineSeparator(
                "ANIMALS", "cow", "dog", "cat", "NEW_CAT", "gold");
        Files.write(input, inputFileContent.getBytes());

        final CategoryDataConsumerStrategy animalsCategoryDataConsumerStrategy = mockCategoryDataConsumerStrategy("ANIMALS");
        final CategoryDataConsumerStrategy newCatCategoryDataConsumerStrategy = mockCategoryDataConsumerStrategy("NEW_CAT");
        List<CategoryDataConsumerStrategy> strategies = Arrays.asList(
                animalsCategoryDataConsumerStrategy, newCatCategoryDataConsumerStrategy
        );
        when(applicationContext.getStrategies()).thenReturn(strategies);

        application.handleFile(absolutePath);

        verify(animalsCategoryDataConsumerStrategy).add(eq("cow"));
        verify(animalsCategoryDataConsumerStrategy).add(eq("dog"));
        verify(animalsCategoryDataConsumerStrategy).add(eq("cat"));
        verify(newCatCategoryDataConsumerStrategy).add(eq("gold"));
    }

    @Test
    public void shouldPrintResultsForTwoStrategies() throws IOException {
        final String animalsCategoryResult = "animalsCategoryResult";
        final String newCatCategoryResult = "newCatCategoryResult";
        final CategoryDataConsumerStrategy animalsCategoryDataConsumerStrategy =
                mockCategoryDataConsumerStrategyWithResultProcessor(animalsCategoryResult);
        final CategoryDataConsumerStrategy newCatCategoryDataConsumerStrategy =
                mockCategoryDataConsumerStrategyWithResultProcessor(newCatCategoryResult);
        List<CategoryDataConsumerStrategy> strategies = Arrays.asList(
                animalsCategoryDataConsumerStrategy, newCatCategoryDataConsumerStrategy
        );
        when(applicationContext.getStrategies()).thenReturn(strategies);
        doCallRealMethod().when(application).printResults();

        application.printResults();

        verify(outputWriter).print(animalsCategoryResult);
        verify(outputWriter).print(newCatCategoryResult);
    }

    private CategoryDataConsumerStrategy mockCategoryDataConsumerStrategy(String categoryName) {
        final CategoryDataConsumerStrategy categoryDataConsumerStrategy = mock(CategoryDataConsumerStrategy.class);
        when(categoryDataConsumerStrategy.match(categoryName)).thenReturn(true);
        return categoryDataConsumerStrategy;
    }

    private CategoryDataConsumerStrategy mockCategoryDataConsumerStrategyWithResultProcessor(String result) {
        final CategoryDataConsumerStrategy categoryDataConsumerStrategy = mock(CategoryDataConsumerStrategy.class);
        CategoryItemsProcessor categoryItemsProcessor = mock(CategoryItemsProcessor.class);
        when(categoryItemsProcessor.getFormattedResult()).thenReturn(result);
        when(categoryDataConsumerStrategy.resultProcessor()).thenReturn(categoryItemsProcessor);
        return categoryDataConsumerStrategy;
    }
}