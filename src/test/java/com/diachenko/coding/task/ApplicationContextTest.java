package com.diachenko.coding.task;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import com.diachenko.coding.task.enums.OutputModes;
import com.diachenko.coding.task.formater.impl.CarsBackwordSortedListResultWriter;
import com.diachenko.coding.task.formater.impl.NumberFrequencyListResultWriter;
import com.diachenko.coding.task.formater.impl.SortedListCategoryItemsProcessor;
import com.diachenko.coding.task.output.OutputWriter;
import com.diachenko.coding.task.output.impl.ConsoleOutputWriter;
import com.diachenko.coding.task.output.impl.FileOutputWriter;
import com.diachenko.coding.task.strategy.CategoryDataConsumerStrategy;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Properties;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationContextTest {

    @Mock
    private ApplicationStartParameters applicationStartParameters;
    @Mock
    private OutputWriter outputWriter;
    @Spy
    private Properties properties;

    private ApplicationContext applicationContext;

    @Before
    public void setUp() {
        when(applicationStartParameters.getOutputMode()).thenReturn(OutputModes.CONSOLE.getName());
        applicationContext = new ApplicationContext(applicationStartParameters);
        applicationContext.initialize();
    }

    @Test
    public void shouldReturnAllPredefinedCategoryStrategies() {
        final List<CategoryDataConsumerStrategy> strategies = applicationContext.getStrategies();

        assertEquals(3, strategies.size());
    }

    @Test
    public void shouldReturnAnimalsCategoryStrategy() {
        final List<CategoryDataConsumerStrategy> strategies = applicationContext.getStrategies();

        final CategoryDataConsumerStrategy categoryDataConsumerStrategy = strategies.get(0);
        assertEquals(SortedListCategoryItemsProcessor.class, categoryDataConsumerStrategy.resultProcessor().getClass());
    }

    @Test
    public void shouldReturnNumbersCategoryStrategy() {
        final List<CategoryDataConsumerStrategy> strategies = applicationContext.getStrategies();

        final CategoryDataConsumerStrategy categoryDataConsumerStrategy = strategies.get(1);
        assertEquals(NumberFrequencyListResultWriter.class, categoryDataConsumerStrategy.resultProcessor().getClass());
    }

    @Test
    public void shouldReturnCarsCategoryStrategy() {
        final List<CategoryDataConsumerStrategy> strategies = applicationContext.getStrategies();

        final CategoryDataConsumerStrategy categoryDataConsumerStrategy = strategies.get(2);
        assertEquals(CarsBackwordSortedListResultWriter.class, categoryDataConsumerStrategy.resultProcessor().getClass());
    }

    @Test
    public void shouldReturnFileOutputWriter() throws IOException {
        when(applicationStartParameters.getOutputMode()).thenReturn(OutputModes.FILE.getName());
        applicationContext = spy(new ApplicationContext(applicationStartParameters));
        when(applicationContext.loadApplicationProperties()).thenReturn(properties);
        File tempFile = Files.createTempFile("input", ".txt").toFile();
        tempFile.deleteOnExit();
        when(properties.getProperty("file.mode.output.file.path")).thenReturn(tempFile.getAbsolutePath());
        applicationContext.initialize();

        final OutputWriter outputWriter = applicationContext.getOutputWriter();

        assertTrue(outputWriter instanceof FileOutputWriter);
    }

    @Test
    public void shouldReturnConsoleOutputWriter() {
        final OutputWriter outputWriter = applicationContext.getOutputWriter();

        assertTrue(outputWriter instanceof ConsoleOutputWriter);
    }

}