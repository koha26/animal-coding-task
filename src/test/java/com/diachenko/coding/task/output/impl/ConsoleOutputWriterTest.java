package com.diachenko.coding.task.output.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ConsoleOutputWriterTest {

    private ConsoleOutputWriter consoleOutputWriter;
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
        consoleOutputWriter = new ConsoleOutputWriter();
    }

    @After
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Test
    public void shouldWriteOneLineMessage() {
        final String expectedMessage = "I am console";
        consoleOutputWriter.print(expectedMessage);

        final String actualMessage = outputStreamCaptor.toString().trim();
        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    public void shouldWriteTwoLinesMessage() {
        final String expectedMessage = "This is a text" + System.lineSeparator()
                + "with line separator";
        consoleOutputWriter.print(expectedMessage);

        final String actualMessage = outputStreamCaptor.toString().trim();
        assertEquals(expectedMessage, actualMessage);
    }
}