package com.diachenko.coding.task.output.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;
import java.util.stream.Collectors;

@RunWith(MockitoJUnitRunner.class)
public class FileOutputWriterTest {

    @Spy
    private Properties properties;

    private FileOutputWriter fileOutputWriter;
    private Path tempFile;

    @Before
    public void setUp() throws IOException {
        tempFile = Files.createTempFile("input", ".txt");
        tempFile.toFile().deleteOnExit();
        when(properties.getProperty("file.mode.output.file.path")).thenReturn(tempFile.toFile().getAbsolutePath());
        fileOutputWriter = new FileOutputWriter(properties);
    }

    @Test
    public void shouldWriteOneLineMessage() throws IOException {
        final String expectedMessage = "I am file";
        fileOutputWriter.print(expectedMessage);

        final String actualOutput = Files.readAllLines(tempFile).stream()
                .collect(Collectors.joining(System.lineSeparator()));
        assertEquals(expectedMessage, actualOutput);
    }

    @Test
    public void shouldWriteTwoLinesMessage() throws IOException {
        final String expectedMessage = "This is a text" + System.lineSeparator()
                + "with line separator";
        fileOutputWriter.print(expectedMessage);

        final String actualOutput = Files.readAllLines(tempFile).stream()
                .collect(Collectors.joining(System.lineSeparator()));
        assertEquals(expectedMessage, actualOutput);
    }
}