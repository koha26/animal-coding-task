package com.diachenko.coding.task.strategy.impl;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

import com.diachenko.coding.task.formater.CategoryItemsProcessor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DummyCategoryParsingStrategyTest {
    private DummyCategoryDataConsumerStrategy categoryParsingStrategy;

    @Before
    public void beforeTest() {
        categoryParsingStrategy = new DummyCategoryDataConsumerStrategy();
    }

    @Test
    public void shouldReturnFalseWhenMatchAnyInput() {
        assertFalse(categoryParsingStrategy.match("any_string"));
    }

    @Test
    public void shouldReturnEmptyString() {
        final CategoryItemsProcessor categoryItemsProcessor = categoryParsingStrategy.resultProcessor();
        assertEquals(0, categoryItemsProcessor.getFormattedResult().length());
    }
}