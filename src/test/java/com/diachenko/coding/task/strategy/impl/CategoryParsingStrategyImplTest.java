package com.diachenko.coding.task.strategy.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

import com.diachenko.coding.task.formater.CategoryItemsProcessor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

@RunWith(MockitoJUnitRunner.class)
public class CategoryParsingStrategyImplTest {

    public static final String EXPECTED_CATEGORY_NAME = "ANIMALS";

    @Mock
    private List<String> items;
    @Mock
    private BiFunction<String, List<String>, CategoryItemsProcessor> resultBuilder;
    @Mock
    private BiPredicate<String, String> matcher;

    @InjectMocks
    private CategoryDataConsumerStrategyImpl categoryParsingStrategy;

    @Before
    public void beforeTest() {
        categoryParsingStrategy = new CategoryDataConsumerStrategyImpl(EXPECTED_CATEGORY_NAME, matcher, resultBuilder);
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldReturnTrueIfCategoryMatch() {
        categoryParsingStrategy.match(EXPECTED_CATEGORY_NAME);

        verify(matcher).test(EXPECTED_CATEGORY_NAME, EXPECTED_CATEGORY_NAME);
    }

    @Test
    public void shouldReturnFalseIfCategoryNotMatch() {
        categoryParsingStrategy = new CategoryDataConsumerStrategyImpl(EXPECTED_CATEGORY_NAME, String::equals, resultBuilder);
        MockitoAnnotations.initMocks(this);

        assertFalse(categoryParsingStrategy.match("animals"));
    }

    @Test
    public void shouldAddItemToList() {
        final String line = "dog";

        categoryParsingStrategy.add(line);

        verify(items).add(line);
    }

    @Test
    public void shouldReturnItemProcessor() {
        categoryParsingStrategy.resultProcessor();

        verify(resultBuilder).apply(eq(EXPECTED_CATEGORY_NAME), eq(items));
    }
}