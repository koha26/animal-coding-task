package com.diachenko.coding.task.formater.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class CarsBackwordSortedListResultWriterTest {

    private static final String EXPECTED_CATEGORY_NAME = "CARS";
    private CarsBackwordSortedListResultWriter target;

    @Test
    public void shouldReturnCarsInReverseOrder() {
        List<String> items = Arrays.asList("lexus", "lada", "lamdorgini", "LADA", "bmw");
        target = new CarsBackwordSortedListResultWriter(EXPECTED_CATEGORY_NAME, items);

        final List<String> lineKeys = Arrays.asList(EXPECTED_CATEGORY_NAME + ":", "lexus", "lamdorgini", "lada", "bmw");
        final String formattedResult = target.getFormattedResult();
        final String[] actualLines = formattedResult.split(System.lineSeparator());

        assertEquals(lineKeys.size(), actualLines.length);
        for (int i = 0; i < lineKeys.size(); i++) {
            assertTrue(actualLines[i].contains(lineKeys.get(i)));
        }
    }

    @Test
    public void shouldReturnUniqueCars() {
        List<String> items = Arrays.asList( "audi", "bmw", "bmw", "BMW", "BmW");
        target = new CarsBackwordSortedListResultWriter(EXPECTED_CATEGORY_NAME, items);

        final List<String> lineKeys = Arrays.asList(EXPECTED_CATEGORY_NAME + ":", "bmw", "audi");
        final String formattedResult = target.getFormattedResult();
        final String[] actualLines = formattedResult.split(System.lineSeparator());

        assertEquals(lineKeys.size(), actualLines.length);
        for (int i = 0; i < lineKeys.size(); i++) {
            assertTrue(actualLines[i].contains(lineKeys.get(i)));
        }
    }

    @Test
    public void shouldHaveSpecialCarLineFormat() {
        List<String> items = Arrays.asList("bmw");
        target = new CarsBackwordSortedListResultWriter(EXPECTED_CATEGORY_NAME, items);

        final List<String> lineKeys = Arrays.asList(EXPECTED_CATEGORY_NAME + ":", "bmw");
        final String formattedResult = target.getFormattedResult();
        final String[] actualLines = formattedResult.split(System.lineSeparator());

        assertEquals(actualLines[0], EXPECTED_CATEGORY_NAME + ":");
        assertTrue(actualLines[1].matches("\\$ bmw \\(.+\\)"));
    }



}