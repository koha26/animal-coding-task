package com.diachenko.coding.task.formater;

import java.util.Arrays;
import java.util.stream.Collectors;

public class TestFormaterUtil {
    public static String joinLinesWithLineSeparator(String... lines) {
        return Arrays.asList(lines).stream()
                .collect(Collectors.joining(System.lineSeparator(), "", System.lineSeparator()));
    }
}
