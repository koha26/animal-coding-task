package com.diachenko.coding.task.formater.impl;

import static com.diachenko.coding.task.formater.TestFormaterUtil.joinLinesWithLineSeparator;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NumberFrequencyListResultWriterTest {

    public static final String EXPECTED_CATEGORY_NAME = "CATEGORY";
    private NumberFrequencyListResultWriter target;

    @Test
    public void shouldReturnUniqueDataWithoutCorrectFrequencies() {
        List<String> items = Arrays.asList("a", "b", "c", "d", "d", "a", "a");
        target = new NumberFrequencyListResultWriter(EXPECTED_CATEGORY_NAME, items);


        String expectedResult = joinLinesWithLineSeparator(EXPECTED_CATEGORY_NAME + ":", "a: 3", "b: 1", "c: 1", "d: 2");
        final String formattedResult = target.getFormattedResult();

        assertEquals(expectedResult, formattedResult);
    }

    @Test
    public void shouldReturnUniqueDataWithoutItemsSorting() {
        List<String> items = Arrays.asList("d", "a", "a", "a");
        target = new NumberFrequencyListResultWriter(EXPECTED_CATEGORY_NAME, items);

        String expectedResult = joinLinesWithLineSeparator(EXPECTED_CATEGORY_NAME + ":", "d: 1", "a: 3");
        final String formattedResult = target.getFormattedResult();

        assertEquals(expectedResult, formattedResult);
    }

    @Test
    public void shouldReturnOnlyFirstLineIfItemsEmpty() {
        List<String> items = Collections.emptyList();
        target = new NumberFrequencyListResultWriter(EXPECTED_CATEGORY_NAME, items);

        String expectedResult = joinLinesWithLineSeparator(EXPECTED_CATEGORY_NAME + ":");
        final String formattedResult = target.getFormattedResult();

        assertEquals(expectedResult, formattedResult);
    }

}