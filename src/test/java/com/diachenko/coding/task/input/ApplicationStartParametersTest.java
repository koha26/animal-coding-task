package com.diachenko.coding.task.input;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.diachenko.coding.task.ApplicationStartParameters;

import org.junit.Test;

public class ApplicationStartParametersTest {

    private ApplicationStartParameters parameters;

    @Test
    public void shouldFillWithDefaultValues() {
        final String[] strings = {};

        parameters = new ApplicationStartParameters(strings);

        assertEquals("console", parameters.getOutputMode());
        assertFalse(parameters.getFilePath().isPresent());
    }

    @Test
    public void shouldReturnOptionalWithNotEmptyFilepath() {
        final String expectedFilePath = "filePath";
        final String[] strings = {expectedFilePath};

        parameters = new ApplicationStartParameters(strings);

        assertTrue(parameters.getFilePath().isPresent());
        assertEquals(expectedFilePath, parameters.getFilePath().get());
    }

    @Test
    public void shouldReturnNotDefaultOutputMode() {
        final String expectedFilePath = "filePath";
        final String expectedOutputMode = "expectedOutputMode";
        final String[] strings = {expectedFilePath, expectedOutputMode};

        parameters = new ApplicationStartParameters(strings);

        assertEquals(expectedOutputMode, parameters.getOutputMode());
    }
}