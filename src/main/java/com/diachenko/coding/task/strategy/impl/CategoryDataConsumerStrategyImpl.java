package com.diachenko.coding.task.strategy.impl;

import com.diachenko.coding.task.formater.CategoryItemsProcessor;
import com.diachenko.coding.task.strategy.CategoryDataConsumerStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

/**
 * Implementation of {@link CategoryItemsProcessor}.
 *
 * @author Kostiantyn_Diachenko
 */
public class CategoryDataConsumerStrategyImpl implements CategoryDataConsumerStrategy {
    private String categoryName;
    private List<String> items;
    private BiFunction<String, List<String>, CategoryItemsProcessor> resultProcessorFactory;
    private BiPredicate<String, String> matcher;

    /**
     * @param categoryName name of the category
     * @param matcher special predicate to match category name
     * @param resultProcessorFactory result processor factory to create result processor
     */
    public CategoryDataConsumerStrategyImpl(String categoryName,
                                            BiPredicate<String, String> matcher,
                                            BiFunction<String, List<String>, CategoryItemsProcessor> resultProcessorFactory) {
        this.categoryName = categoryName;
        this.resultProcessorFactory = resultProcessorFactory;
        this.matcher = matcher;
        items = new ArrayList<>();
    }

    @Override
    public boolean match(String item) {
        return matcher.test(categoryName, item);
    }

    @Override
    public void add(String item) {
        items.add(item);
    }

    @Override
    public CategoryItemsProcessor resultProcessor() {
        return resultProcessorFactory.apply(categoryName, items);
    }
}
