package com.diachenko.coding.task.strategy;

import com.diachenko.coding.task.formater.CategoryItemsProcessor;

/**
 * Represents a strategy for parsing category items. One strategy per one category.
 *
 * @author Kostiantyn_Diachenko
 */
public interface CategoryDataConsumerStrategy {

    /**
     * Matches start of category.
     *
     * @param item line (potential category marker)
     * @return true if item matches category start
     */
    boolean match(String item);

    /**
     * Consumes category item.
     *
     * @param item category item
     */
    void add(String item);

    /**
     * Returns result processor.
     *
     * @return result processor
     */
    CategoryItemsProcessor resultProcessor();
}
