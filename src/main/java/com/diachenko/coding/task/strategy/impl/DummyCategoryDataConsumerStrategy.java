package com.diachenko.coding.task.strategy.impl;

import com.diachenko.coding.task.formater.CategoryItemsProcessor;
import com.diachenko.coding.task.strategy.CategoryDataConsumerStrategy;

/**
 * Stub-implementation of {@link CategoryDataConsumerStrategy}.
 *
 * @author Kostiantyn_Diachenko
 */
public class DummyCategoryDataConsumerStrategy implements CategoryDataConsumerStrategy {

    @Override
    public boolean match(String item) {
        return false;
    }

    @Override
    public void add(String item) {
        // Do nothing
    }

    @Override
    public CategoryItemsProcessor resultProcessor() {
        return String::new;
    }
}
