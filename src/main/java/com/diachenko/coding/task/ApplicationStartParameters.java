package com.diachenko.coding.task;

import com.diachenko.coding.task.enums.OutputModes;

import java.util.Optional;

/**
 * Application start command parameters.
 * First parameter - file path (required).
 * Second parameter - output type (console|file).
 *
 * @author Kostiantyn_Diachenko
 */
public class ApplicationStartParameters {
    private static final int FILE_PATH_INDEX = 0;
    private static final int OUTPUT_TYPE_INDEX = 1;

    private Optional<String> filePath;
    private String outputMode;

    public ApplicationStartParameters(String[] args) {
        filePath = Optional.of(args).filter(a -> a.length > FILE_PATH_INDEX).map(a -> a[FILE_PATH_INDEX]);
        outputMode = Optional.of(args).filter(a -> a.length > OUTPUT_TYPE_INDEX).map(a -> a[OUTPUT_TYPE_INDEX])
                .orElse(OutputModes.CONSOLE.getName());
    }

    public Optional<String> getFilePath() {
        return filePath;
    }

    public String getOutputMode() {
        return outputMode;
    }

}
