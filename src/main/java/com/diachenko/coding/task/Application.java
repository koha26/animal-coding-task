package com.diachenko.coding.task;

import com.diachenko.coding.task.formater.CategoryItemsProcessor;
import com.diachenko.coding.task.strategy.CategoryDataConsumerStrategy;
import com.diachenko.coding.task.strategy.impl.DummyCategoryDataConsumerStrategy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.Consumer;

/**
 * Entrypoint to application.
 *
 * @author Kostiantyn_Diachenko
 */
public class Application {
    private static final Logger LOGGER = LogManager.getLogger(Application.class);
    private static final Consumer<String> NEW_CATEGORY_LINE_CONSUMER = line -> {/* do not save header line */};
    private static final String CHARSET_NAME = "UTF-8";

    private ApplicationContext context;

    public Application(ApplicationStartParameters applicationStartParameters) {
        context = new ApplicationContext(applicationStartParameters);
    }

    public void start() {
        context.initialize();
        context.getStartParameters().getFilePath().ifPresent(this::handleFile);
    }

    protected void handleFile(String filePath) {
        CategoryDataConsumerStrategy currentStrategy = new DummyCategoryDataConsumerStrategy();
        try (
                FileInputStream inputStream = new FileInputStream(filePath);
                Scanner scanner = new Scanner(inputStream, CHARSET_NAME)) {

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();

                Optional<CategoryDataConsumerStrategy> newStrategy = context.getStrategies().stream()
                        .filter(strategy -> strategy.match(line))
                        .findFirst();

                Consumer<String> lineConsumer = newStrategy
                        .map(strategy -> NEW_CATEGORY_LINE_CONSUMER)
                        .orElse(currentStrategy::add);
                lineConsumer.accept(line);
                currentStrategy = newStrategy.orElse(currentStrategy);
            }

            printResults();
        } catch (IOException e) {
            LOGGER.error("Occurred while trying to read a file", e);
        }
    }

    protected void printResults() {
        context.getStrategies().stream()
                .map(CategoryDataConsumerStrategy::resultProcessor)
                .map(CategoryItemsProcessor::getFormattedResult)
                .forEach(context.getOutputWriter()::print);
    }

    public static void main(String[] args) {
        new Application(new ApplicationStartParameters(args)).start();
    }
}
