package com.diachenko.coding.task.utils;

import java.util.function.BiPredicate;

/**
 * @author Kostiantyn_Diachenko
 */
public class HeaderMatchers {
    private HeaderMatchers() {
    }

    public static BiPredicate<String, String> caseInsensetive() {
        return String::equalsIgnoreCase;
    }
}
