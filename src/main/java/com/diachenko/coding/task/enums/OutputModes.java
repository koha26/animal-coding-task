package com.diachenko.coding.task.enums;

/**
 * Describes all available output modes.
 *
 * @author Kostiantyn_Diachenko
 */
public enum OutputModes {
    CONSOLE("console"),
    FILE("file");

    private String name;
    OutputModes(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
