package com.diachenko.coding.task.enums;

/**
 * Describes all category names.
 *
 * @author Kostiantyn_Diachenko
 */
public enum Categories {
    ANIMALS("ANIMALS"),
    NUMBERS("NUMBERS"),
    CARS("CARS");

    private String name;
    Categories(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
