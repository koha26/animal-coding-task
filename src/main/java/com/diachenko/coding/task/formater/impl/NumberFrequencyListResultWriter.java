package com.diachenko.coding.task.formater.impl;

import com.diachenko.coding.task.formater.AbstractCategoryItemsProcessor;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Represents data processor for NUMBERS category.
 * Format: A list of all unique data items of category “NUMBERS”,
 * followed by “: “ and the number of occurrences of the respective data item.
 * The order is preserved in the order of appearance.
 *
 * @author Kostiantyn_Diachenko
 */
public class NumberFrequencyListResultWriter extends AbstractCategoryItemsProcessor {

    private static final String NUMBER_FREQUENCY_STRING_FORMAT = "%s: %d";

    public NumberFrequencyListResultWriter(String header, List<String> items) {
        super(header, items);
    }

    @Override
    public String getFormattedResult() {
        StringBuilder result = new StringBuilder(getHeader()).append(":").append(System.lineSeparator());
        getTextFrequencyMap().entrySet().stream()
                .map(entry -> String.format(NUMBER_FREQUENCY_STRING_FORMAT, entry.getKey(), entry.getValue()))
                .forEach(item -> result.append(item).append(System.lineSeparator()));

        return result.toString();
    }

    private LinkedHashMap<String, Long> getTextFrequencyMap() {
        return getItems().stream().collect(Collectors.groupingBy(
                Function.identity(),
                LinkedHashMap::new,
                Collectors.mapping(Function.identity(), Collectors.counting())
        ));
    }
}
