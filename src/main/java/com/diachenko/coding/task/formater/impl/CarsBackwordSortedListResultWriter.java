package com.diachenko.coding.task.formater.impl;

import com.diachenko.coding.task.formater.AbstractCategoryItemsProcessor;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Comparator;
import java.util.List;

/**
 * Represents data processor for CARS category.
 * Format: the unique data items of category “CARS” as a backwards sorted list, each item written in lower case.
 * Each data item output shall be followed by the item’s MD5 hash written in brackets.
 *
 * @author Kostiantyn_Diachenko
 */
public class CarsBackwordSortedListResultWriter extends AbstractCategoryItemsProcessor {

    private static final String CAR_LINE_RESULT_FORMAT = "$ %s (%s)";

    public CarsBackwordSortedListResultWriter(String header, List<String> items) {
        super(header, items);
    }

    @Override
    public String getFormattedResult() {
        StringBuilder result = new StringBuilder(getHeader()).append(":").append(System.lineSeparator());
        getItems().stream()
                .map(String::toLowerCase)
                .distinct()
                .sorted(Comparator.reverseOrder())
                .map(item -> String.format(CAR_LINE_RESULT_FORMAT, item, DigestUtils.md5Hex(item)))
                .forEach(item -> result.append(item).append(System.lineSeparator()));
        return result.toString();
    }
}
