package com.diachenko.coding.task.formater;

/**
 * Processes parsed (collected) category items and returns formatted result.
 *
 * @author Kostiantyn_Diachenko
 */
public interface CategoryItemsProcessor {
    /**
     * Returns result in format is described by implementers.
     *
     * @return formatted results
     */
    String getFormattedResult();
}
