package com.diachenko.coding.task.formater.impl;

import com.diachenko.coding.task.formater.AbstractCategoryItemsProcessor;

import java.util.List;

/**
 * Represents data processor for data which should be presented as sorted list of unique elements with leading category name.
 *
 * @author Kostiantyn_Diachenko
 */
public class SortedListCategoryItemsProcessor extends AbstractCategoryItemsProcessor {

    public SortedListCategoryItemsProcessor(String header, List<String> items) {
        super(header, items);
    }

    @Override
    public String getFormattedResult() {
        StringBuilder result = new StringBuilder(getHeader()).append(":").append(System.lineSeparator());
        getItems().stream()
                .distinct()
                .sorted()
                .forEach(item -> result.append(item).append(System.lineSeparator()));
        return result.toString();
    }


}
