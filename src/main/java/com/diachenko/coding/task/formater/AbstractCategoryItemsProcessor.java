package com.diachenko.coding.task.formater;

import java.util.List;

/**
 * @author Kostiantyn_Diachenko
 */
public abstract class AbstractCategoryItemsProcessor implements CategoryItemsProcessor {
    private String header;
    private List<String> items;

    public AbstractCategoryItemsProcessor(String header, List<String> items) {
        this.header = header;
        this.items = items;
    }

    public String getHeader() {
        return header;
    }

    public List<String> getItems() {
        return items;
    }
}
