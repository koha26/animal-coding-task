package com.diachenko.coding.task.output;

/**
 * Custom writer for results.
 *
 * @author Kostiantyn_Diachenko
 */
public interface OutputWriter {
    void print(String output);
}
