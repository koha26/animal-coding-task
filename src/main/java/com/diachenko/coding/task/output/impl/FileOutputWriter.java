package com.diachenko.coding.task.output.impl;

import com.diachenko.coding.task.output.OutputWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Properties;

/**
 * Result-writer which prints all data to the separate file.
 * File path is configured by application.properties file.
 *
 * @author Kostiantyn_Diachenko
 */
public class FileOutputWriter implements OutputWriter {
    private static final Logger LOGGER = LogManager.getLogger(FileOutputWriter.class);

    private String outputFileName;
    final Path path;

    public FileOutputWriter(Properties properties) {
        outputFileName = properties.getProperty("file.mode.output.file.path");
        path = Paths.get(outputFileName);
    }

    @Override
    public void print(String output) {
        try {
            Files.write(path, output.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException e) {
            LOGGER.error("Couldn't write content to file", e);
            throw new UncheckedIOException(e);
        }
    }
}
