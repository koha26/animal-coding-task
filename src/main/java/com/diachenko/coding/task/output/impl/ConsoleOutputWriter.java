package com.diachenko.coding.task.output.impl;

import com.diachenko.coding.task.output.OutputWriter;

/**
 * Result-writer which prints all data to the console.
 *
 * @author Kostiantyn_Diachenko
 */
public class ConsoleOutputWriter implements OutputWriter {
    @Override
    public void print(String output) {
        System.out.println(output);
    }
}
