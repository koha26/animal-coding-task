package com.diachenko.coding.task;

import com.diachenko.coding.task.enums.Categories;
import com.diachenko.coding.task.enums.OutputModes;
import com.diachenko.coding.task.formater.impl.CarsBackwordSortedListResultWriter;
import com.diachenko.coding.task.formater.impl.NumberFrequencyListResultWriter;
import com.diachenko.coding.task.formater.impl.SortedListCategoryItemsProcessor;
import com.diachenko.coding.task.output.OutputWriter;
import com.diachenko.coding.task.output.impl.ConsoleOutputWriter;
import com.diachenko.coding.task.output.impl.FileOutputWriter;
import com.diachenko.coding.task.strategy.CategoryDataConsumerStrategy;
import com.diachenko.coding.task.strategy.impl.CategoryDataConsumerStrategyImpl;
import com.diachenko.coding.task.utils.HeaderMatchers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.function.Supplier;

/**
 * Application context - is responsible for storing all services/properties/factories, etc.
 *
 * @author Kostiantyn_Diachenko
 */
public class ApplicationContext {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationContext.class);
    private static final String APPLICATION_PROPERTIES_FILE_NAME = "application.properties";

    private ApplicationStartParameters startParameters;
    private List<CategoryDataConsumerStrategy> strategies;
    private Map<String, Supplier<OutputWriter>> outputWriterFactoryFactory;
    private OutputWriter outputWriter;
    private Properties properties;

    public ApplicationContext(ApplicationStartParameters parameters) {
        startParameters = parameters;
    }

    public void initialize() {
        properties = loadApplicationProperties();
        outputWriterFactoryFactory = initializeOutputWriterFactoryFactory();
        strategies = initializeStrategies();
        outputWriter = getOutputWriterFactory(startParameters).get();
    }

    public List<CategoryDataConsumerStrategy> getStrategies() {
        return strategies;
    }

    public OutputWriter getOutputWriter() {
        return outputWriter;
    }

    public ApplicationStartParameters getStartParameters() {
        return startParameters;
    }

    protected Properties loadApplicationProperties() {
        Properties applicationProperties = new Properties();
        try {
            applicationProperties.load(getClass().getClassLoader().getResourceAsStream(APPLICATION_PROPERTIES_FILE_NAME));
        } catch (IOException e) {
            LOGGER.error("Couldn't load properties.", e);
        }
        return applicationProperties;
    }

    private Map<String, Supplier<OutputWriter>> initializeOutputWriterFactoryFactory() {
        final Map<String, Supplier<OutputWriter>> factoryFactoryMap = new HashMap<>();
        factoryFactoryMap.put(OutputModes.CONSOLE.getName(), ConsoleOutputWriter::new);
        factoryFactoryMap.put(OutputModes.FILE.getName(), () -> new FileOutputWriter(properties));
        return factoryFactoryMap;
    }

    private Supplier<OutputWriter> getOutputWriterFactory(ApplicationStartParameters parameters) {
        return outputWriterFactoryFactory.entrySet().stream()
                .filter(entry -> entry.getKey().equalsIgnoreCase(parameters.getOutputMode()))
                .map(Map.Entry::getValue)
                .findFirst()
                .orElse(ConsoleOutputWriter::new);
    }

    private List<CategoryDataConsumerStrategy> initializeStrategies() {
        return Collections.unmodifiableList(Arrays.asList(
                new CategoryDataConsumerStrategyImpl(Categories.ANIMALS.getName(),
                        HeaderMatchers.caseInsensetive(), SortedListCategoryItemsProcessor::new),
                new CategoryDataConsumerStrategyImpl(Categories.NUMBERS.getName(),
                        HeaderMatchers.caseInsensetive(), NumberFrequencyListResultWriter::new),
                new CategoryDataConsumerStrategyImpl(Categories.CARS.getName(),
                        HeaderMatchers.caseInsensetive(), CarsBackwordSortedListResultWriter::new)
        ));
    }
}
